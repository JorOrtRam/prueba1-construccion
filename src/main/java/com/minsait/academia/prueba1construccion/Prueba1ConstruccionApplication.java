package com.minsait.academia.prueba1construccion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Prueba1ConstruccionApplication {

    public static void main(String[] args) {
        SpringApplication.run(Prueba1ConstruccionApplication.class, args);
    }

}
