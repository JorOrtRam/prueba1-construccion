package com.minsait.academia.prueba1construccion.productos;

import java.util.Date;

public class Product {

    /*Campos: BRAND_ID: foreign key de la cadena del grupo (1 = Scappino).
START_DATE, END_DATE: rango de fechas en el que aplica el precio tarifa indicado.
PRICE_LIST: Identificador de la tarifa aplicable.
PRODUCT_ID: Identificador código de producto.
PRIORITY: Desambiguador de aplicación de precios. Si dos tarifas coinciden en un rango de fechas se aplica la de mayor prioridad (mayor valor numérico)
PRICE: precio final de venta.
CURR: ISO de la moneda+/  */

    private Integer brandId;

    private Date startDate;

    private Date endDate;

    private Integer priceList;

    private Integer productId;

    private Integer priority;

    private Double price;

    private String curr;

    public Product(Integer brandId, Date startDate, Date endDate, Integer priceList, Integer productId,
                   Integer priority, Double price, String curr) {
        this.brandId = brandId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceList = priceList;
        this.productId = productId;
        this.priority = priority;
        this.price = price;
        this.curr = curr;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Integer getPriceList() {
        return priceList;
    }

    public Integer getProductId() {
        return productId;
    }

    public Integer getPriority() {
        return priority;
    }

    public Double getPrice() {
        return price;
    }

    public String getCurr() {
        return curr;
    }

    @Override
    public String toString() {
        return "ProductosBean{" +
                "brandId=" + brandId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", priceList=" + priceList +
                ", productId=" + productId +
                ", priority=" + priority +
                ", price=" + price +
                ", curr='" + curr + '\'' +
                '}';
    }
}
