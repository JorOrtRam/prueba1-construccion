package com.minsait.academia.prueba1construccion.productos;

import com.minsait.academia.prueba1construccion.excepciones.ProductNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductResource {

    private final ProductService service;

    public ProductResource(ProductService service) {
        this.service = service;
    }

    // GET /products
    // retrieveAllProducts
    @GetMapping("/products")
    public List<Product> retrieveAllProducts() {
        return service.findAll();
    }

    // retrieveProduct(int priceList)
    @GetMapping("/products/{id}")
    public Product retrieveProduct(@PathVariable int priceList) throws ProductNotFoundException {
        Product product = service.findOne(priceList);
        if (product == null) {
            throw new ProductNotFoundException("id- " + priceList + " has an error.");
        }
        return product;

    }
}
