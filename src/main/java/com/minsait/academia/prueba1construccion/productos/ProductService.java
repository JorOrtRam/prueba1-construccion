package com.minsait.academia.prueba1construccion.productos;

import org.apache.catalina.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ProductService {

    private static List<Product> product = new ArrayList<>();

    private static int productCount = 4;

    static{
        product.add(new Product(1,new Date(), new Date(), 1, 35455, 0, 35.50,"EUR"));
        product.add(new Product(1,new Date(), new Date(), 2, 35455, 1, 25.45,"EUR"));
        product.add(new Product(1,new Date(), new Date(), 3, 35455, 1, 30.50,"EUR"));
        product.add(new Product(1,new Date(), new Date(), 4, 35455, 1, 38.95,"EUR"));
    }

    public List<Product> findAll() {
        return product;
    }

    public Product findOne(int id) {
        for (Product product : product) {
            if (product.getProductId() == id) {
                return product;
            }
        }
        return null;
    }
}
